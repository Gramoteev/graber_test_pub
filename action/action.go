package action

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strings"
	"time"

	"github.com/mmcdole/gofeed/rss"
	"golang.org/x/net/html"

	"graber/config"
	"graber/logger"
	db "graber/postgresql"
)

type SitesGrabers struct {
	Sites []Site
}

type Site struct {
	Mode int // 0 - обычный грабер, 1 -через RSS

	ContainerName  string //Основной контейнер
	ContainerClass string //Класс основного контейнера
	ContainerID    string //Или ID контейнера

	ArticleBodyTag  string //Класс Тела статьи
	ArticleTitleTag string //Класс заголовка статьи

	Link //Настройки маршрута по которому идти

	RSS //Если мод rss, то укажите настройки

	SaveDataKey int64
}

type Link struct {
	Attr     string
	LinkSite string
	StartKey int
	Dop      string

	Count int
}

type RSS struct {
	Link, DopAttr string
	LastBuildDate string
}

var (
	reg  = regexp.MustCompile(`(<!--[^\"]*-->)`)
	reg2 = regexp.MustCompile(`[^А-Яа-я ]+`)
)

//([^А-Яа-я ]+) | (<!--[^\"]*-->)  -- не помню я как объединять условия...
func (sg *SitesGrabers) Start() error {

	if !config.Config.Settings.HistoryArticls {
		sg.RemoveData()
	}

	logger.Application.Println("Начало парсинга сайта")

nextSite:
	for _, site := range sg.Sites {

		//Проверим нужно ли учитывать историю?
		if config.Config.Settings.HistoryArticls {
			_keys, err := History(site.Link.LinkSite)
			if err != nil {
				logger.Errors.Println("История пустая! Продолжаем с настройками по умолчанию")
			} else {
				if _keys > 0 {
					site.Link.StartKey = _keys
					site.Link.StartKey++ //И берем следующий
					logger.Application.Println("Новый ID:", site.Link.StartKey)
				}
			}
		}

		if site.Mode == 0 { //Обычный грабер

			for i := 0; i < site.Link.Count; i++ {

				//Получим основной контейнер с указаными параметрами для поиска
				bodyByte, err := site.download(fmt.Sprintf("%s%s%d%s", site.Link.LinkSite, site.Link.Attr, site.Link.StartKey, site.Link.Dop))

				if err != nil {
					logger.Errors.Println(err.Error())
					site.Link.StartKey++
					if config.Config.Settings.AllList {
						continue
					}
					continue nextSite
				}

				err = sg.ParseArticls(&site, bodyByte)
				if err != nil {
					logger.Errors.Println("Ошибка Вставки:", err.Error())
					site.Link.StartKey++
					continue
				}
				//Найдем, всё что нам нужно

				site.Link.StartKey++
			}
		} else if site.Mode == 1 { // испеользуя ленту
			XMLBody, err := sg.ParseRss(&site)
			if err != nil {
				logger.Errors.Println(err.Error())
				continue nextSite
			}
			if XMLBody == nil {
				continue nextSite
			}
			for _, item := range XMLBody.Items {
				bodyByte, err := site.download(item.Link)
				if err != nil {
					logger.Errors.Println(err.Error())
					if config.Config.Settings.AllList {
						continue
					}
					continue nextSite
				}
				err = sg.ParseArticls(&site, bodyByte)
				if err != nil {
					logger.Errors.Println("Ошибка Вставки:", err.Error())
					continue
				}
			}
		}

		//Прошли указанное кол-во раз по сайту, надо ли продолжать работать автономно?
		if config.Config.Settings.NextArticls {
			go sg.NextArticls(site) //Объект не ссылка, копернется нормально.
		}

	}
	return nil
}

//RemoveData - Обнуление истории (бд)
func (sg *SitesGrabers) RemoveData() {
	if err := db.Requests.ExecTransact("Delete.ArticlsTest."); err != nil {
		logger.Errors.Println("Ошибка Удаления:", err.Error())
	}
}

//ParseArticls - Парсинг тела разметки страницы
func (sg *SitesGrabers) ParseArticls(site *Site, bodyByte *html.Node) error {
	titleArt, err := site.Search(bodyByte, site.ArticleTitleTag)
	if err != nil {
		return errors.New("Ошибка поиска заголовка:" + err.Error())
	}

	bodyArt, err := site.Search(bodyByte, site.ArticleBodyTag)
	if err != nil {
		return errors.New("Ошибка поиска тела статьи:" + err.Error())
	}

	titleExec := reg2.ReplaceAllString(reg.ReplaceAllString(site.renderNode(titleArt), ""), "")
	bodyExec := reg2.ReplaceAllString(reg.ReplaceAllString(site.renderNode(bodyArt), ""), "")

	_Key := site.Link.StartKey
	if site.Mode == 1 {
		_Key = -1
	}
	if err = db.Requests.ExecTransact("Insert.ArticlsTest.", site.Link.LinkSite, _Key, titleExec, bodyExec); err != nil {
		return err
	}
	return nil
}

//NextArticls - метод позволяет, продолжить сбор информации
func (sg *SitesGrabers) NextArticls(site Site) {

	for {
		time.Sleep(5 * time.Second)

		//logger.Application.Println("Выводим объект от которого считаем:", fmt.Sprintf("%+v", site))
		if site.Mode == 0 { //TODO: Внутри можно увеличить количество итерация для сбора (пропуск 404)
			bodyByte, err := site.download(fmt.Sprintf("%s%s%d%s", site.Link.LinkSite, site.Link.Attr, site.Link.StartKey, site.Link.Dop))
			if err != nil {
				logger.Errors.Println(err.Error())
				site.Link.StartKey++
				continue
			}

			err = sg.ParseArticls(&site, bodyByte)
			if err != nil {
				logger.Errors.Println(err.Error())
				site.Link.StartKey++
				continue
			}
			site.Link.StartKey++

		} else if site.Mode == 1 { // испеользуя ленту
			XMLBody, err := sg.ParseRss(&site)
			if err != nil {
				logger.Errors.Println(err.Error())
				continue
			}
			if XMLBody == nil {
				continue
			}
			for _, item := range XMLBody.Items {
				bodyByte, err := site.download(item.Link)
				if err != nil {
					logger.Errors.Println("Ошибка Вставки:", err.Error())
					continue
				}
				err = sg.ParseArticls(&site, bodyByte)
				if err != nil {
					logger.Errors.Println("Ошибка Вставки:", err.Error())
					continue
				}
			}
		}

	}
}

//ParseRss - Парсим местную xml разметку, если выбран режим 1
func (sg *SitesGrabers) ParseRss(site *Site) (*rss.Feed, error) {
	time.Sleep(5 * time.Second)
	if len(site.RSS.Link) == 0 {
		return nil, errors.New("Не задан параметр RSS, для сайта:" + site.LinkSite)
	}

	logger.Application.Println("Ищем:" + site.LinkSite + site.RSS.Link + site.RSS.DopAttr)
	res, err := http.Get(site.LinkSite + site.RSS.Link + site.RSS.DopAttr)
	if err != nil {
		return nil, errors.New("Искомый файл за " + site.LinkSite + site.RSS.Link + site.RSS.DopAttr + " не обнаружен [error]:" + err.Error())
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, errors.New("Пустой поток в xml")
	}

	stringData := string(body)
	rssParser := rss.Parser{}
	rssXML, _ := rssParser.Parse(strings.NewReader(stringData))

	if len(site.LastBuildDate) == 0 {
		site.LastBuildDate = rssXML.LastBuildDate
	} else if site.LastBuildDate == rssXML.LastBuildDate {
		logger.Application.Println("LastBuildDate совпадает действий не требуется")
		return nil, nil
	} else {
		site.LastBuildDate = rssXML.LastBuildDate
	}
	return rssXML, nil
}
