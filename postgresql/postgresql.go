package postgresql

// Работа с postgresql

import (
	"database/sql"
	"errors"
	"fmt"
	Config "graber/config"

	_ "github.com/lib/pq"
)

var db *sql.DB
var Requests dbRequests

type dbRequests struct {
	requestsList map[string]*sql.Stmt
}

func (dbr *dbRequests) initRequests() error {

	dbr.requestsList = make(map[string]*sql.Stmt)

	var err error

	dbr.requestsList["Insert.ArticlsTest."], err = db.Prepare(`INSERT INTO articlsTest(domen, id_art, title_art, body_art ) VALUES ($1, $2, $3, $4) ON CONFLICT DO NOTHING`)
	if err != nil {
		return err
	}

	dbr.requestsList["Delete.ArticlsTest."], err = db.Prepare(`Delete from articlsTest`)
	if err != nil {
		return err
	}

	dbr.requestsList["Select.ArticlsTest."], err = db.Prepare(`Select id, title_art, body_art from articlsTest where title_art like '%' || $1 || '%'`)
	if err != nil {
		return err
	}

	dbr.requestsList["Select.ArticlsTest.LastOfDomen"], err = db.Prepare(`SELECT id_art FROM public.articlstest where domen=$1 order by  id  desc limit 1`)
	if err != nil {
		return err
	}

	return err
}

func (dbr *dbRequests) ExecTransact(requestName string, values ...interface{}) error {

	req, ok := dbr.requestsList[requestName]
	if !ok {
		return errors.New("Missmatch request!")
	}

	var err error
	_, err = req.Exec(values...)
	if err != nil {
		return err
	}

	return nil
}

type Request struct {
	RequestName string
	Values      []interface{}
}

func (dbr *dbRequests) QueryRow(requestName string, values ...interface{}) (*sql.Row, error) {

	req, ok := dbr.requestsList[requestName]
	if !ok {
		return nil, errors.New("Missmatch request!")
	}

	var row *sql.Row
	row = req.QueryRow(values...)
	return row, nil

}

func (dbr *dbRequests) Query(requestName string, values ...interface{}) (*sql.Rows, error) {

	req, ok := dbr.requestsList[requestName]
	if !ok {
		return nil, errors.New("Missmatch request!")
	}

	var rows *sql.Rows
	var err error
	rows, err = req.Query(values...)

	return rows, err

}

//Тут паника в основном потоке
func init() {
	var err error
	fmt.Println("Подключаем и инициализируем запросы БД")
	db, err = sql.Open("postgres", "host="+Config.Config.DB.Host+" port="+Config.Config.DB.Port+" user="+Config.Config.DB.Login+" password="+Config.Config.DB.Pass+" dbname="+Config.Config.DB.DB+" sslmode="+Config.Config.DB.SSL)
	if err != nil {
		Config.Config.DB.Host = "localhost"
		db, err = sql.Open("postgres", "host="+Config.Config.DB.Host+" port="+Config.Config.DB.Port+" user="+Config.Config.DB.Login+" password="+Config.Config.DB.Pass+" dbname="+Config.Config.DB.DB+" sslmode="+Config.Config.DB.SSL)
		if err != nil {
			fmt.Println("Postgresql not found!:", err)
			panic(err)
		}
	}
	if err = db.Ping(); err != nil {
		fmt.Println("Postgresql not reply!:", err)
		panic(err)
	}
	fmt.Println("Подключаем к БД выполнено")
	if err := Requests.initRequests(); err != nil {
		fmt.Println(err)
		panic(err)
	}
	fmt.Println("Запросы БД инициализированы")
	fmt.Println("БД Успешно подключена")
}
