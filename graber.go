package main

import (
	"graber/action"
	"graber/router"

	"github.com/gin-gonic/gin"
)

func main() {

	var _sites []action.Site
	_sites = append(_sites, action.Site{
		Mode:           0, //rss
		ContainerName:  "div",
		ContainerClass: "newsblock",
		ContainerID:    "",

		ArticleTitleTag: "newsh",
		ArticleBodyTag:  "newsstat",
		Link: action.Link{
			LinkSite: "http://prodonzokt.ru",
			Attr:     "/news-",
			StartKey: 224771,
			Count:    1,
		},
	})

	_sites = append(_sites, action.Site{
		Mode:           0, //rss
		ContainerName:  "div",
		ContainerClass: "main-container",
		ContainerID:    "",

		ArticleTitleTag: "news-title",
		ArticleBodyTag:  "current-article js-mediator-article",
		Link: action.Link{
			LinkSite: "https://www.bfm.ru",
			Attr:     "/news/",
			StartKey: 4227,
			Dop:      "",
			Count:    2,
		},
	})

	_sites = append(_sites, action.Site{
		Mode:           1, //rss
		ContainerName:  "div",
		ContainerClass: "news-item-content",
		ContainerID:    "",

		ArticleTitleTag: "headline",
		ArticleBodyTag:  "post-text",
		Link: action.Link{
			LinkSite: "https://www.seonews.ru",
			Attr:     "/news/",
			StartKey: 4227,
			Dop:      "",
			Count:    1,
		},
		RSS: action.RSS{
			Link:    "/rss/",
			DopAttr: "events",
		},
	})

	sg := action.SitesGrabers{
		Sites: _sites,
	}

	err := sg.Start()
	if err != nil {
		panic(err)
	}

	r := gin.Default()
	r.LoadHTMLGlob("templates/*")
	r.Static("/assets", "./assets")
	router.InitRouter(r)

	r.Run()
}
