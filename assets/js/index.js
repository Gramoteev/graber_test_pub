function start() {
    let ss = document.getElementById("search").value;
    console.log(ss);
    document.getElementById("records").innerHTML="";
    var zapros = new XMLHttpRequest();
    zapros.open('GET', '/Search/Title/'+ss, false);
    zapros.send();
    if (zapros.status != 200) {
        console.log("Ошибка")
    }

    let answer = JSON.parse(zapros.responseText);

    if (answer.Error == "null") {
        console.log("Ошибка:", answer.Error)
        return 
    } 

    let div = document.createElement('div');

    for (let i=0; i<answer.Records.length;i++){
        div.innerHTML += `<h1>${answer.Records[i].Title}</h1><p>${answer.Records[i].Body}</p><hr>`;
    }
    document.getElementById("records").append(div);

}