Table:
```
CREATE TABLE public.articlstest
(
    id serial,
    domen character varying NOT NULL,
    id_art integer NOT NULL,
    title_art character varying NOT NULL,
    body_art character varying NOT NULL,
    CONSTRAINT articlstest_domen_title_art_key UNIQUE (domen, title_art)
)
```

Грабер работает как обычный грабер страниц (по ссылкам), так и по rss файлу, для этого необходимо выставить mode в основной структуре и сконфигурировать теги согласно описанию в коде, сейчас настроено 2-3 сайта.