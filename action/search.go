package action

import (
	"graber/postgresql"
)

type SearchStruct struct {
	ID          int
	Title, Body string
}

func SearchTitle(title string) (ss []SearchStruct, err error) {

	rows, err := postgresql.Requests.Query("Select.ArticlsTest.", title)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		_ss := SearchStruct{}
		if err = rows.Scan(&_ss.ID, &_ss.Title, &_ss.Body); err != nil {
			return
		}
		ss = append(ss, _ss)
	}
	return
}

func History(domen string) (int, error) {
	var lastID int
	row, err := postgresql.Requests.QueryRow("Select.ArticlsTest.LastOfDomen", domen)
	if err != nil {
		return 0, err
	}
	if err := row.Scan(&lastID); err != nil {
		return 0, err
	}
	return lastID, nil
}
