package router

import (
	"graber/action"

	"github.com/gin-gonic/gin"
)

func InitRouter(r *gin.Engine) {

	r.GET("/", func(c *gin.Context) {
		c.HTML(200, "index", gin.H{})
	})

	titleGr := r.Group("/Search")

	titleGr.GET("/Title/:Text", SearchTitleHandler)

}

func SearchTitleHandler(c *gin.Context) {

	strText := c.Param("Text")
	H := gin.H{
		"Error": nil,
	}

	var err error
	H["Records"], err = action.SearchTitle(strText)
	if err != nil {
		H["Error"] = "Ошибка получения данных"
		return
	}

	c.JSON(200, H)
}
