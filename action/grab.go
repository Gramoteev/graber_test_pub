package action

import (
	"bytes"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	"golang.org/x/net/html"

	"graber/logger"
)

func (self *Site) download(link string) (*html.Node, error) {
	logger.Application.Println("Смотрим:", link)
	res, err := http.Get(link)
	if err != nil {
		return nil, errors.New("Искомый файл за " + link + " не обнаружен [error]:" + err.Error())
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	doc, err := html.Parse(strings.NewReader(string(body)))
	if err != nil {
		return nil, err
	}
	var bodyHtml *html.Node

	if len(self.ContainerID) > 0 && len(self.ContainerClass) > 0 {
		return nil, errors.New("Поиск по ID  и классу одновременно не доступен")
	}

	var _keyConteiner string = self.ContainerID
	var _valConteiner string = "id"
	if len(self.ContainerClass) > 0 {
		_keyConteiner = self.ContainerClass
		_valConteiner = "class"
	}

	var f func(*html.Node)
	f = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == self.ContainerName {
			for _, val := range n.Attr {
				if val.Key == _valConteiner && val.Val == _keyConteiner {
					bodyHtml = n
				}
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)

	if bodyHtml != nil {
		return bodyHtml, nil
	}

	return nil, errors.New("Ошибка парсинга")
}

//Search - осуществляет поиск интересующей нас информации
func (self *Site) Search(doc *html.Node, uniqParam string) (result *html.Node, err error) {

	var f func(*html.Node)
	f = func(n *html.Node) {
		if n.Type == html.ElementNode {
			for _, val := range n.Attr {
				if val.Val == uniqParam {
					result = n
				}
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)
	if result != nil {
		return result, nil
	}
	return nil, errors.New("Данный атрибут не найден")
}

func (self *Site) renderNode(n *html.Node) string {
	var buf bytes.Buffer
	html.Render(io.Writer(&buf), n)
	return buf.String()
}
