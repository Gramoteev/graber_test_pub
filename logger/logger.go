package logger

import (
	"fmt"
	"log"
	"os"
	"runtime"
	"time"
)

type chanBase struct {
	c        chan string
	fileName string
}

type chanApplication struct {
	chanBase
}

type chanErrors struct {
	chanBase
}

var (
	logApplication *log.Logger
	logErrors      *log.Logger
)

var (
	//Application - вывод инфы приложения
	Application = chanApplication{chanBase: chanBase{c: make(chan string)}}
	//Errors - вывод инфы ошибок
	Errors = chanErrors{chanBase: chanBase{c: make(chan string)}}
)

var logFile *os.File

func initLogs() {
	logFile.Close()

	dir := "./log/" + time.Now().String()[:10]

	_, err := os.Stat(dir)
	if os.IsNotExist(err) {
		os.MkdirAll(dir, 0777)
	}

	logFile, err = os.OpenFile(dir+"/global.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Panic("Logfile not found!:", err)
	}
	log.SetOutput(logFile)

	Application.fileName = dir + "/application.log"
	logFile, err = os.OpenFile(Application.fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	logApplication = log.New(logFile, "", log.Ldate|log.Ltime) //|log.Lshortfile

	Errors.fileName = dir + "/errors.log"
	logFile, err = os.OpenFile(Errors.fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	logErrors = log.New(logFile, "", log.Ldate|log.Ltime)
}

//Тут тоже паника в основном потоке
func init() {
	initLogs()

	go recLog()
	go fromChannel()
}

func recLog() {
	t1 := time.Now()
	t2, err := time.Parse("2006-01-02T15:04:05.000000-07:00", t1.String()[0:10]+"T23:59:59.999999+05:00")
	if err != nil {
		return
	}
	time.Sleep(time.Minute)
	time.Sleep(t2.Sub(t1.Add(time.Minute * 3)))
	initLogs()
	recLog()
}

func fromChannel() {
	for {
		select {
		case x, ok := <-Application.c:
			if !ok {
				Application.c = nil
				continue
			}
			logApplication.Print(x)

		case x, ok := <-Errors.c:
			if !ok {
				Errors.c = nil
				continue
			}
			logErrors.Print(x)

		}
	}
}

func (ch *chanApplication) Println(values ...interface{}) {
	if ch.c != nil {
		ch.c <- fmt.Sprintln(values...)
	}
}

func (ch *chanErrors) Println(values ...interface{}) {
	if ch.c != nil {
		pc, file, line, ok := runtime.Caller(1)
		if !ok {
			ch.c <- fmt.Sprintln(values...)
		} else {
			ch.c <- fmt.Sprintln(values, fmt.Sprintf("Файл: %s:%d (0x%x)\n", file, line, pc))
			ch.c <- fmt.Sprintln(values, fmt.Sprintf("Файл: %s:%d (0x%x)\n", file, line, pc))
		}

	}
}
