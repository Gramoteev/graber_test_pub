package main

import (
	"fmt"
	"graber/action"
	"testing"
)

func TestSearchTitle(t *testing.T) {

	list, err := action.SearchTitle("к")
	if err != nil {
		panic(err)
	}

	if len(list) > 0 {
		fmt.Printf("%+v", list)
		fmt.Println("Поиск работает, данные есть:", len(list))
	}
}
