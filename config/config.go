package config

// Получение настроек из файла конфигураций

import (
	"fmt"

	"github.com/spf13/viper"
)

//Объявление структуры конфигураций.
type Configurations struct {
	DB struct {
		Login, Pass string
		Host, Port  string
		DB          string
		SSL         string
	}
	Settings struct {
		AllList        bool //Флаг определяет останавливаться ли? При ошибке парсинга страниц при запуске, или продолжать указанное кол-во раз
		HistoryArticls bool //Учитывать ли историю?
		NextArticls    bool //Продолжать сбор?
	}
}

type DBConfig struct {
	Postgre_user      string
	Postgre_password  string
	Postgre_host      string
	Postgre_port      string
	Postgre_database  string
	Postgre_sslmodeDB string
}

func (c *Configurations) getConfigurations() error {

	viper.AddConfigPath("config")

	viper.SetConfigName("config")

	// поиск и чтение конфиг файла
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	err = viper.Unmarshal(&Config)

	if err != nil {
		panic("Unable to decode into struct:" + err.Error())
	}

	return nil
}

var Config Configurations

//Тут тоже паника
func init() {

	// Чтение файла конфигураций
	err := Config.getConfigurations()
	if err != nil {
		fmt.Println("Конфигурационный файл не найден:", err)
		panic(err)
	}
	fmt.Println("Файл конфигурации:", fmt.Sprintf("%+v", Config))
}
